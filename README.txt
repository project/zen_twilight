
Zen Twilight
----------

Project Page:
http://drupal.org/project/zen_twilight

By Garrett Albright
http://drupal.org/user/191212
Email: albright (at) anre [dot] net
AIM: AlbrightGuy - ICQ: 150670553 - IRC: Albright


A simple Drupal theme inspired by the dark blues of a clear summer twilight.

NOTICE: This is a Zen subtheme. You MUST install the Zen theme before Zen
Twilight will work:
http://drupal.org/project/zen

See the Project page for more information.

